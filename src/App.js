/* eslint-disable */
import React from 'react';
import './App.css';
import { Form, Input, Button } from 'antd';
import { Layout } from 'antd';
import { Typography } from 'antd';
import axios from 'axios';


const { Header, Content } = Layout;
const { Title } = Typography;

export class App extends React.Component {

  state = {
    title: 'TEST',
    body: ' '
  };

  handleChange = ({target}) =>{
    const { name, value } = target; 
    
    this.setState({
     [ name ]: value });
  };

  onSubmit = (even) => {
    even.preventDefault();

    
    const payload = {
      title: this.state.title,
      body: this.state.body

    };

    axios.post({
      method: 'POST',
      url: 'http://localhost:3000/note_routes/save',
      data: payload
    })
    .then(() => {
      console.log('Les donnees envoyees au serveur.');
      this.resetUserInputs();
    })
    .catch(()=> {
      console.log('Erreur');
    });
  };

  resetUserInputs = () =>{
    this.setState({
      title: '',
      body: ''
    });
  };

  clickMe (body) {
    console.log(body.title);
  }

  render(){
    console.log();
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };

  return (
    <div className="App">
      <Layout>
        <Header style={{background:'white'}}>
        </Header>
        <Content style={{background:'white'}}>
          <Form onSubmit={this.onSubmit} style={{margin:0}} {...layout} >
            <Form.Item  label=" ">
            <div className="form-input">
              <Input
               type="text" 
               name="title"
               value={this.state.title}
                />
              </div>
            </Form.Item>
            <Form.Item  label=" ">
            <div className="form-input">
            <Form.Item>
         <Input.TextArea 
         name="body"
         placeholder="Ecrire ici"
         rows={10}
         value={this.state.body}
         onChange={e => this.setState({body: e.target.value})}
         />
      </Form.Item>
            </div>

            </Form.Item >
            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
              <Button type="primary" htmlType = " submit " onClick={ axios} >
                Save
            </Button>
            </Form.Item>
          </Form>
        </Content>
      </Layout>
    </div>
    
  )};
};

export default App;



